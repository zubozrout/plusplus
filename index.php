<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Zubozrout Plus | Martin Kozub's Google Plus backup Feed</title>
        <link rel="stylesheet" type="text/css" href="tool/css/ui.css">
        <link rel="stylesheet" type="text/css" href="tool/css/feed.css">
        <link rel="stylesheet" type="text/css" href="tool/css/admin.css">
        <?php
            if(isset($_GET["about"])) {
        ?>
        <link rel="stylesheet" type="text/css" href="tool/css/about.css">
        <?php
            }
        ?>
        <link rel="icon" href="tool/assets/plus.png">
        
        <meta name="description" content="Martin Kozub's original Google Plus backup Feed.">
        <meta name="keywords" content="GooglePlus, Google+, Posts, Martin Kozub">
        <meta name="author" content="Martin Kozub">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Extended" rel="stylesheet">
    </head>
    <body>
        <header>
            <?php
                if(!isset($_GET["about"])) {
            ?>
            <div class="left-content">
                <div class="hamburger" title="History">
                    <svg width="24px" height="24px" viewBox="0 0 48 48">
                        <path d="M6 36h36v-4H6v4zm0-10h36v-4H6v4zm0-14v4h36v-4H6z"></path>
                    </svg>
                </div>
                <?php
                    }
                ?>
                <a class="logo" href=".">Zubozrout Plus</a>
            </div>
            
            <div class="user-account"></div>
        </header>
        
        <?php
            if(isset($_GET["about"])) {
                include("tool/server/about.php");
            }
            else {
        ?>
        
        <div class="container">
            <div class="list"></div>
            <div class="feed-container">
                <div class="feed">
                    <?php
                        if(isset($_GET["display-post"])) {
                            require_once("tool/server/visor.php");
                            echo $visor->getPost(base64_decode($_GET["display-post"]));
                        }
                    ?>
                </div>
            </div>
        </div>
        
        <script>
            "use strict";
            document.querySelector("a.logo").addEventListener("click", (event) => {
                event.preventDefault();
                history.pushState("", document.title, window.location.pathname + window.location.search);
                plus.feed.selectView();
            });
            document.querySelector(".hamburger").addEventListener("click", (event) => {
                document.querySelector(".list").classList.toggle("visible");
            });
        </script>
        <script src="tool/js/post.js"></script>
        <script src="tool/js/single-source.js"></script>
        <script src="tool/js/feed.js"></script>
        <script src="tool/js/plus.js"></script>
        <?php
            }
        ?>
    </body>
</html>
