<?php
class PostTemplate {
    private $content;
    private $userHTMLContent;
    
    function __construct($content) {
        $this->content = $content;
        $this->prepareUserHTML();
    }
    
    function getContent() {
        return $this->content;
    }
    
    function prepareUserHTML() {
        $isPublic = $this->content->isPublic;
        $postGoogle = $this->content->postGoogle;
        $url = $this->content->url;
        $server = $this->content->server;
        
        $authorName = $this->content->author->displayName;
        $authorUrl = $this->content->author->profilePageUrl;
        $authorImageUrl = $this->content->author->avatarImageUrl;
        
        $creationTime = $this->content->creationTime;
        $updateTime = $this->content->updateTime;
        $postDate = $updateTime ? $updateTime : $creationTime;
        
        $postContent = $this->content->content;
        $link = $this->content->link;
        $resharedPost = $this->content->resharedPost;
        
        $headContentA = "<a class=\"author\" title=\"$authorName\" href=\"$authorUrl\"><img src=\"$authorImageUrl\"><span>$authorName</span></a>";
        $headContentB = "<a class=\"date\" href=\"$postUrl\"><span class=\"date-label\">$postDate</span></a>";
        $headContainer = "<div class=\"head\">\n\t$headContentA\n\t$headContentB\n</div>";
        $articleContent = "<div class=\"content\"><p>$postContent</p></div>";
        $this->userHTMLContent = "<article class=\"post\">$headContainer\n$articleContent\n</article>";
    }    
    
    function getUserHTML() {
        return $this->userHTMLContent;
    }
}
?>
