<?php
require_once("server.php");
require_once("post-template.php");

class Visor {
    function __construct() {
        global $administration;
        $this->administration = $administration;
    }
    
    function getPost($fileName) {
        $postTemplate = new PostTemplate($this->administration->getRawPostContent($fileName));

        if($postTemplate === "file-not-found") {
            echo "fileNotFound";
        }
        else if($postTemplate === "permission-denied") {
            echo "accessNotGranted";
        }
        else {
            return $postTemplate->getUserHTML();
        }
    }
}

$visor = new Visor();
?>
