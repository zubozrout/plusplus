<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=utf-8");

// Always change working directory to the current's script working directory
chdir(dirname(__FILE__));

class Posts {
    private $path = "../../posts/";
    private $extension = ".json";
    private $match = "*.json";
    private $authenticated = false;
    private $author = null;
    
    function __construct($authenticated) {
        $this->authenticated = $authenticated;
    }
    
    function getCount() {
        $count = array(
            "authenticated" => $this->authenticated,
            "count" => 0
        );
        
        if($this->authenticated) {
            $count["count"] = count(glob($this->path.$this->match));
        }
        else {
            foreach(glob($this->path.$this->match) as $file) {
                $decodedPost = json_decode(file_get_contents($file));
                if($decodedPost->postAcl->isPublic) {
                    $count["count"]++;
                }
            }
        }
        return json_encode($count);
    }
    
    function getPostsPaths($skip = 0, $limit = 0) {
        $list = array(
            "authenticated" => $this->authenticated,
            "list" => array(),
            "pages" => 0,
            "skip" => $skip,
            "limit" => $limit,
            "count" => 0
        );
        
        $files = array_reverse(glob($this->path.$this->match));
        if($this->authenticated) {
            $list["count"] = count($files);
            
            if($limit > 0) {
                $list["pages"] = ceil(sizeof($files) / $limit);
                $files = array_slice($files, $skip, $limit);
            }
            
            foreach($files as $file) {
                array_push($list["list"], basename($file, $this->extension));
            }
        }
        else {
            $publicPosts = array();
            foreach($files as $file) {
                $decodedPost = json_decode(file_get_contents($file));
                if($decodedPost->postAcl->isPublic) {
                    array_push($publicPosts, $file);
                }
            }
            
            $list["count"] = count($publicPosts);
            
            if($limit > 0) {
                $list["pages"] = ceil(sizeof($publicPosts) / $limit);
                $publicPosts = array_slice($publicPosts, $skip, $limit);
            }
            
            foreach($publicPosts as $publicPost) {
                array_push($list["list"], basename($publicPost, $this->extension));
            }
        }
        return json_encode($list);
    }
    
    function getRawPostContent($fileName) {
        $fileName .= $this->extension;
        if(file_exists($this->path.$fileName)) {
            $decodedPost = json_decode(file_get_contents($this->path.$fileName));
            $isPublic = $decodedPost->postAcl->isPublic;
            if($isPublic || $this->authenticated) {
                $decodedPost->isPublic = $isPublic;
                return $decodedPost;
            }
            return "permission-denied";
        }
        return "file-not-found";
    }
    
    function getPostContent($fileName) {
        $decodedPost = $this->getRawPostContent($fileName);
        if($decodedPost === "file-not-found") {
            $postContent = json_encode(array("error" => "fileNotFound"));
        }
        else if($decodedPost === "permission-denied") {
            $postContent = json_encode(array("error" => "accessNotGranted"));
        }
        else {
            $updates = [];
            $updates["isPublic"] = $decodedPost->isPublic;
            $updates["postGoogle"] = $decodedPost->postGoogle;
            $updates["url"] = $decodedPost->url;
            $updates["server"] = $decodedPost->server;
            $updates["author"] = $decodedPost->author;
            $updates["creationTime"] = $decodedPost->creationTime;
            $updates["updateTime"] = $decodedPost->updateTime;
            $updates["content"] = $decodedPost->content;
            $updates["link"] = $decodedPost->link;
            $updates["resharedPost"] = $decodedPost->resharedPost;
            $postContent = json_encode($updates);
            
            $this->author = $decodedPost->author; // Save author for post creation
        }
        return $postContent;
    }
    
    function deletePost($fileName) {
        if($this->authenticated) {
            $postContent = json_encode(array("error" => "fileNotFound"));
            $fileName .= $this->extension;
            if(file_exists($this->path.$fileName)) {
                $postContent = file_get_contents($this->path.$fileName);
                $decodedPost = json_decode($postContent);
                
                if($decodedPost->postGoogle) {
                    unlink($this->path.$fileName) or $postContent = json_encode(array("error" => "couldNotDeleteFile"));
                }
                else {
                    $postContent = json_encode(array("error" => "contentNotDeletable"));
                }
            }
            return $postContent;
        }
        return json_encode(array("error" => "accessNotGranted"));
    }
    
    function findAndMakeURL($string) {
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        if(preg_match($reg_exUrl, $string, $url)) {
            return preg_replace($reg_exUrl, "<a href=\"". $url[0] . "\">" . $url[0] . "</a>", $string);
        }
        else {
            return $string;
        }
    }
    
    function findAndMakeNewLine($string) {
        return str_replace(array("\r\n", "\n", "\r"), "<br>", $string);
    }
    
    function textFormating($string) {
        $string = $this->findAndMakeURL($string);
        $string = $this->findAndMakeNewLine($string);
        return $string;
    }
    
    function savePost($data) {
        if($this->authenticated) {
            $dateTime = DateTime::createFromFormat("U.u", microtime(true));
            $id = $data->id ? $data->id : $dateTime->format("YmdGisu") . "-ng";            
            $fileName = $this->path . $id . $this->extension;
                        
            $postContent = array();
            $postContent["id"] = $id;
            $postContent["postGoogle"] = true;
            $postContent["server"] = $data->server;
            $postContent["author"] = $data->author;
            $postContent["creationTime"] = $data->creationTime ? $data->creationTime : $dateTime->format("Y-m-d G:i:s");
            $postContent["updateTime"] = $dateTime->format("Y-m-d G:i:s");
            $postContent["content"] = $data->content; //$this->textFormating($data->content);
            $postContent["link"] = $data->link;
            $postContent["resharedPost"] = $data->resharedPost;
            $postContent["postAcl"] = $data->postAcl;
            
            $jsonStringToSave = json_encode($postContent);
            file_put_contents($fileName, $jsonStringToSave);
            return $jsonStringToSave;
        }
    }
}

class Administration {
    private $config = [];
    
    private $expectedCredentials = "";
    private $passedCredentials = "";
    
    public $authenticated = false;
    
    private $posts;
    
    function __construct() {
        $this->getAccountConfig("../config.ini");
        $this->expectedCredentials = $this->getUserKey();
        
        if(isset($_GET["login"])) {
            echo $this->confirmRawCredentials();
            return;
        }
        
        // We have a user name + password hash
        // Let's find out if what users sends us matches
        $this->passedCredentials = isset($_GET["key"]) ? rawurldecode($_GET["key"]) : "";
        
        // Compare and desice if user has access to provate data
        $this->authenticated = $this->expectedCredentials === $this->passedCredentials;
        
        $this->posts = new Posts($this->authenticated);
        
        if(isset($_FILES["file"])) {
            echo $this->saveFile();
        }
        else {
            if(isset($_GET["get-config"])) {
                echo $this->getConfig();
            }
            else if(isset($_GET["get-count"])) {
                echo $this->posts->getCount();
            }
            else if(isset($_GET["post-id"])) {
                echo $this->posts->getPostContent(rawurldecode($_GET["post-id"]));
            }
            else if(isset($_GET["list-all"])) {
                $skip = isset($_GET["skip"]) ? (int)$_GET["skip"] : 0;
                $limit = isset($_GET["limit"]) ? (int)$_GET["limit"] : 0;
                
                echo $this->posts->getPostsPaths($skip, $limit);
            }
            else if(isset($_GET["meta"])) {
                echo $this->getPageMetadata($_GET["meta"]);
            }
            
            if($this->authenticated && $_SERVER["REQUEST_METHOD"] === "POST") {
                $json_str = file_get_contents("php://input");
                $json_obj = json_decode($json_str);
                if($json_obj->create) {
                    echo $this->posts->savePost($json_obj->create);
                }
                else if($json_obj->delete) {
                    echo $this->posts->deletePost($json_obj->delete);
                }
            }
            else if($_SERVER["REQUEST_METHOD"] === "POST") {
                echo "Creating or deleting post";
            }
        }
    }
    
    function getConfig() {
        if($this->authenticated) {
            $data = parse_ini_file("../config.ini", true);
            return json_encode($data);
        }
        return json_encode(array("error" => "accessNotGranted"));
    }
    
    function getAccountConfig($fileName) {
        $this->config = parse_ini_file($fileName);
        return $this->config;
    }
    
    function getUserKey() {
        $username = $this->config["username"] ? $this->config["username"] : "";
        $password = $this->config["password"] ? $this->config["password"] : "";
        return hash("sha256", $username . $password);
        // return password_hash($username . $password, PASSWORD_DEFAULT);
    }
    
    function confirmRawCredentials() {
        if(isset($_POST["username"]) && isset($_POST["password"])) {
            $username = $this->config["username"] ? $this->config["username"] : "";
            $password = $this->config["password"] ? $this->config["password"] : "";
            
            if($_POST["username"] === $username && $_POST["password"] === $password) {
                return json_encode(array("key" => $this->getUserKey()));
            }
        }
        return json_encode(array("error" => "invalidPassword"));
    }
    
    function getRawPostContent($fileName) {
        return $this->posts->getRawPostContent($fileName);
    }
    
    function getPageMetadata($url) {
        if($this->authenticated) {
            $parsedURL = parse_url(rawurldecode($url));
            $urlHost = $parsedURL["scheme"] . "://" . $parsedURL["host"];
            
            // Load page
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            // curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7 GTB6 (.NET CLR 3.0.4506.2152)");
            // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_URL, rawurldecode($url));
            $content = curl_exec($ch);
            curl_close($ch);
            
            // Parse as HTML
            $doc = new DOMDocument;
            libxml_use_internal_errors(true);
            $convertedToUTF = mb_convert_encoding($content, "HTML-ENTITIES", "UTF-8"); 
            $doc->loadHTML($convertedToUTF);
            libxml_clear_errors();
            
            $xpath = new DOMXPath($doc);
            
            $ogDescription = $xpath->query("//meta[@property=\"og:description\"]/@content")->item(0)->textContent;
            $stDescription = $xpath->query("//meta[@name=\"description\"]/@content")->item(0)->textContent;
            
            $ogImage = $xpath->query("//meta[@property=\"og:image\"]/@content")->item(0)->textContent;
            $images = $xpath->query("//img/@src");
            
            $allImages = array();
            foreach ($xpath->query("//img/@src") as $item) {
                $link = utf8_encode($item->nodeValue);
                
                if(substr($link, 0, 2) === "//") {
                    $link = $parsedURL["scheme"] . ":" . $link;
                }
                else if(substr($link, 0, 4) !== "http") {
                    if(substr($link, 0, 1) === "/") {
                        $link = $urlHost . $link;
                    }
                    else {
                        $link = $urlHost . $parsedURL["path"] . $link;
                    }
                }
                
                if(!empty($link)) {
                    array_push($allImages, $link);
                }
            }
            
            return json_encode(array(
                "url" => rawurldecode($url),
                "title" => $xpath->query("//title")->item(0)->textContent,
                "description" => empty($ogDescription) ? $stDescription : $ogDescription,
                "imageUrl" => empty($ogDescription) && count($allImages) > 0 ? $allImages[0] : $ogImage,
                "pageImages" => $allImages
            ));
        }
        return json_encode(array("error" => "accessNotGranted"));
    }
    
    function saveFile() {
        if($this->authenticated) {
            $uploaddir = "../../uploads/";
            if(!file_exists($uploaddir)) {
                mkdir($uploaddir, 0775, true);
            }
            
            $tmpUploadedFileName = $_FILES["file"]["tmp_name"];
            
            $existingFiles = glob($uploaddir . "*");
            foreach($existingFiles as $existingFile) {
                if(sha1_file($existingFile) === sha1_file($tmpUploadedFileName)) {
                    return json_encode(array("success" => "fileAlreadyExisted", "basename" => $existingFile));
                }
            }
            
            $extension = pathinfo($tmpFile, PATHINFO_EXTENSION);
            
            $newFileName = count($existingFiles) . "-file-upload-" . basename($_FILES["file"]["name"]);
            $destination = $uploaddir . $newFileName;
            
            if(move_uploaded_file($tmpUploadedFileName, $destination)) {
                return json_encode(array("success" => "fileUploaded", "basename" => $newFileName));
            }
            return json_encode(array("error" => "fileNotUploaded", "basename" => $newFileName));
        }
        return json_encode(array("error" => "accessNotGranted"));
    }
}

$administration = new Administration();

?>
