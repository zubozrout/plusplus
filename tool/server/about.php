<div class="container about">
    <section class="overview">
        <div class="wrapper">
            <div id="photo"></div>
            <p>Hello, My name is</p>
            <h1>Martin Kozub</h1>
            <p>
                <span class="nickname">zubozrout</span>
                <span class="short-desc">Web developer, programmer, writer, Linux user</span>
                <ul class="links">
                    <li><a href=".">Google+ feed</a></li>
                    <li><a href="https://www.linkedin.com/in/martin-kozub-98990360">LinkedIn</a></li>
                </ul>
            </p>
        </div>
    </section>
    <div class="plus-image"></div>
    <section class="detail">
        <div class="wrapper">
            <article class="info">
                <p>Hi, my name is Martin Kozub (nickname zubozrout). I am from the Czech Republic and I study and live in Prague. Although I have many interests, they mostly involve computers, however I also love travelling, hiking, drawing, soldering my little experiments, eating exotic food and fruit and music. I like playing strategy and simulation games and I am a passionate Linux user.</p>
                <h2>I am skilled at</h2>
                <ul class="skills">
                    <li>Creating Websites including HTML, CSS, JavaScript, PHP, design, responsive web design, MySQL/MariaDB.</li>
                    <li>Developing in C, C++ and Python, including graphical frontends like GTK, Qt and Qt/QML.</li>
                    <li>Working with Linux both at a user and administrator level.</li>
                    <li>Writing articles.</li>
                </ul>
                <h2>I speak</h2>
                <ul class="languages">
                    <li>Czech (native speaker)</li>
                    <li>English</li>
                    <li>French (basics)</li>
                </ul>
                <h2>Education</h2>
                <ul class="graduation">
                    <li>Graduate at Charles University in Prague (Information Technology), Master's degree.</li>
                </ul>
                <h2>I am passionate about</h2>
                <ul class="passion">
                    <li>Linux, open-source, open-standards etc.</li>
                    <li>Specifically Ubuntu Touch. Yes, it is still very much alive though UBports community :).</li>
                    <li>Traveling, exploring new places and tasting various exotic foods.</li>
                    <li>I love web development and programming. When working on a project I need a space to breath though but also hate to sit on one thing for much too long so I like it when I have variety to what I am doing.</li>
                </ul>
            </article>
        </div>
    </section>
</div>
