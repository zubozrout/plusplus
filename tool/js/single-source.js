"use strict";

class SingleSource {
    constructor(data) {
        this.index = data.index;
        this.plus = data.plus || {};
        this.server = data.server || ".";
        this.passQuery = data.passQuery || false;
        this.paginateBy = data.paginateBy || 10;
        
        this.authenticated = false;
        
        this.onLoad = data.onLoad || null;
        this.onUpdate = data.onUpdate || null;
        this.onError = data.onError || null;
        
        this.count = 0;
        this.skip = 0;
        this.list = [];
        
        this.init();
    }
    
    async init() {
        await this.getCount();
        await this.fetchData({ skip: this.skip });
        if(this.onLoad) {
            this.onLoad(this.list, this);
        }
    }
    
    async loadMore() {
        this.skip += this.paginateBy;
        await this.fetchData({ skip: this.skip });
        if(this.onUpdate) {
            this.onUpdate(this.list, this, this.skip);
        }
    }
    
    async genericFetch(url) {
        const response = await fetch(url);
        if(!response.ok) {
            throw new Error(`An error occred while accessing ${url} resource: ${response.status}`);
        }
        return await response.json();
    }
    
    getGenericUrl(destination) {
        let url = this.plus.getServerComponent(this.server);
        if(this.server === this.plus.mainServer) {
            url += "?key=" + this.plus.key;
        }
        url += (url.match(/\?/) ? "&" : "?") + destination;
        
        if(this.passQuery) {
            url += url.match(/\?/) ? window.location.search.replace(/\?/, "&") : window.location.search;
        }
        
        return url;
    }
    
    async getCount() {
        try {
            const parsedResponse = await this.genericFetch(this.getGenericUrl("get-count"));
            this.authenticated = parsedResponse.authenticated;
            this.count = parsedResponse.count;
        }
        catch(error) {
            this.count = 0;
            console.error(`Failed getting post count, ${error}`);
            if(this.onError) {
                this.onError(error, this);
            }
        }
        return this.count;
    }
    
    async fetchData({ skip = 0, limit = this.paginateBy }) {
        try {
            const parsedResponse = await this.genericFetch(this.getGenericUrl(`list-all&skip=${skip}&limit=${limit}`));
            this.authenticated = parsedResponse.authenticated;
            
            const newItems = parsedResponse.list.filter(newListItem => {
                return !this.list.find(oldListItem => oldListItem.itemId === newListItem);
            });
            
            
            this.list = [
                ...this.list,
                ...(newItems.map(listItem => ({
                    itemId: listItem,
                    source: this
                })))
            ];
        }
        catch(error) {
            console.error(`Failed getting posts data, ${error}`);
            if(this.onError) {
                this.onError(error, this);
            }
        }
        return this.list;
    }
    
    removePost(data) {
        if(data.id) {
            this.list = this.list.filter((post) => {
                return post.itemId !== data.id;
            });
            history.pushState("", document.title, window.location.pathname + window.location.search);
            
            if(this.onUpdate) {
                this.onUpdate(this.list, this);
            }
        }
    }
    
    addPost(data) {
        if(data.id) {
            this.list.push({
                itemId: data.id,
                source: this
            });
            
            if(this.onUpdate) {
                this.onUpdate(this.list, this);
            }
        }
    }
    
    updatePost(data) {
        if(data.id) {
            if(this.onUpdate) {
                this.onUpdate(this.list, this);
            }
        }
    }
}
