"use strict";

class Plus {
    constructor(data) {
        data = data || {};
        
        this.container = data.container || document.querySelector(".container") || document.body;
        this.feedElement = this.container.querySelector(".feed") || document.body;
        this.listElement = this.container.querySelector(".list") || document.body;
        
        this.serverComponent = "/tool/server/";
        this.mainServer = window.location.protocol + "//" + window.location.hostname + window.location.pathname.replace(/\/$/, "");
        this.subscribeList = [];
        
        this.author = {
            displayName: "Post Google Plus",
            profilePageUrl: this.mainServer,
            avatarImageUrl: "plus.png"
        };
        
        this.feed = null;
        
        this.authenticated = false;
        this.key = localStorage.getItem(this.mainServer + "-key") || null;
        
        this.initialComposeText = "What's new?";
        
        this.init();
    }
    
    init() {
        this.getConfig().then((data) => {
            this.startUser(data);
            
            let servers = [];
            servers.push(this.mainServer);
            servers = servers.concat(this.subscribeList);
            
            this.feed = new Feed({
                plus: this,
                servers: servers,
                feed: this.feedElement
            });
            
            if(!data.error && this.key) {
                this.authenticate();
            }
        });
    }
    
    authenticate() {
        this.authenticated = true;
        localStorage.setItem(this.mainServer + "-key", this.key);
        this.update();
    }
    
    deAuthenticate() {
        this.authenticated = false;
        localStorage.removeItem(this.mainServer + "-key");
        this.subscribeList = []; // Reset subscribe list
        this.update();
    }
    
    /*
    setAuthor(data) {
        if(!data.reshared && !this.author) {
            this.author = data.author;
        }
        this.update();
    }
    */
    
    getServerComponent(server) {
        return server + this.serverComponent;
    }
    
    serverRequest(data) {
        data = data || {}; // queryString, content, method, contentType
        
        return new Promise((resolve, reject) => {
            let url = this.getServerComponent(this.mainServer) + (this.key ? "?key=" + this.key : "");
            if(data.queryString) {
                url += url.match(/\?/) ? data.queryString.replace(/\?/, "&") : data.queryString;
            }
            url += url.match(/\?/) ? window.location.search.replace(/\?/, "&") : window.location.search;
            
            const fetchContent = {
                method: data.method || "post",
                headers: {
                    "Accept": "application/json, text/plain, */*"
                }
            };
            
            if(data.contentType) {
                fetchContent.headers["Content-Type"] = data.contentType;
            }
            
            if(fetchContent.method === "post") {
                fetchContent.body = data.content || {};
            }
            
            fetch(url, fetchContent).then((response) => {
                return response.json();
            }).then((parsedResponse) => {
                resolve(parsedResponse);
            }).catch((error) => {
                console.error("Unable to fetch data.");
                reject(error);
            });
        });
    }
    
    getConfig() {
        return new Promise((resolve, reject) => {
            this.serverRequest({
                queryString: "?get-config",
                method: "get"
            }).then((data) => {
                if(data.user) {
                    this.author.displayName = data.user.displayName || this.author.displayName;
                    this.author.avatarImageUrl = data.user.avatarImageUrl || this.author.avatarImageUrl;
                }
                
                if(data.settings) {
                    (data.settings.subscriptions || []).forEach((subscription) => {
                        this.subscribeList.push(subscription);
                    });
                }
                resolve(data);
            }).catch((error) => {
                console.error("Unable to fetch Plus config");
                reject(error);
            });
        });
    }
    
    startUser(data) {
        data = data || {};
        
        const userAccountArea = document.querySelector(".user-account");
        if(userAccountArea) {
            userAccountArea.innerHTML = "";
            
            data.user = data.user || {};
            
            const userButton = document.createElement("div");
            userButton.classList.add("user-button");
            
            const content = document.createElement("div");
            content.classList.add("content");
            
            if(!data.error) {
                const avatarImage = document.createElement("img");
                avatarImage.src = data.user.avatarImageUrl;
                avatarImage.alt = "profile-photo-for-" + data.user.username;
                userButton.appendChild(avatarImage);
                
                const userDisplayName = document.createElement("span");
                userDisplayName.innerText = data.user.displayName;
                content.appendChild(userDisplayName);
            }
            
            const logOutButton = document.createElement("div");
            logOutButton.classList.add("log-in-out");
            logOutButton.innerText = data.error ? "Log in" : "Log out";
            content.appendChild(logOutButton);
            
            userButton.appendChild(content);
            userAccountArea.appendChild(userButton);
            
            logOutButton.addEventListener("click", (event) => {
                if(this.authenticated) {
                    this.key = null;
                    this.deAuthenticate();
                    this.getConfig().then((data) => {
                        this.startUser(data);
                        
                        let servers = [];
                        servers.push(this.mainServer);
                        servers = servers.concat(this.subscribeList);
                        
                        this.feed.servers = servers;
                        this.feed.reset();
                    });
                }
                else {
                    document.body.appendChild(this.createLogInForm());
                }
            });
        }
    }
    
    createLogInForm() {
        const logInForm = document.createElement("div");
        logInForm.classList.add("log-in-form");
        
        const form = document.createElement("form");
        
        const cancelButton = document.createElement("div");
        cancelButton.classList.add("cancel");
        cancelButton.innerText = "Cancel";
        cancelButton.title = "Cancel";
        form.appendChild(cancelButton);
        
        cancelButton.addEventListener("click", (event) => {
            document.body.removeChild(logInForm);
        });
        
        const logInHeader = document.createElement("h2");
        logInHeader.innerText = "Log in to Plus";
        form.appendChild(logInHeader);
        
        const usernameLabel = document.createElement("label");
        usernameLabel.innerText = "User name";
        usernameLabel.for = "username";
        form.appendChild(usernameLabel);
        
        const usernameInput = document.createElement("input");
        usernameInput.name = "username";
        usernameInput.type = "text";
        form.appendChild(usernameInput);
        
        const passwordLabel = document.createElement("label");
        passwordLabel.innerText = "Password";
        passwordLabel.for = "password";
        form.appendChild(passwordLabel);
        
        const passwordInput = document.createElement("input");
        passwordInput.name = "password";
        passwordInput.type = "password";
        form.appendChild(passwordInput);
        
        const submitButton = document.createElement("input");
        submitButton.value = "Log In";
        submitButton.type = "submit";
        form.appendChild(submitButton);
        
        logInForm.appendChild(form);
        
        logInForm.addEventListener("submit", (event) => {
            event.preventDefault();
            logInForm.classList.add("pending");
            logInForm.classList.remove("error");
            
            this.serverRequest({
                queryString: "?login",
                method: "post",
                content: new FormData(event.target)
            }).then((data) => {
                data = data || {};
                
                if(data.key) {
                    this.key = data.key;
                    this.authenticate();
                    this.getConfig().then((configData) => {
                        this.startUser(configData);
                        
                        let servers = [];
                        servers.push(this.mainServer);
                        servers = servers.concat(this.subscribeList);
                        
                        this.feed.servers = servers;
                        this.feed.reset();
                    });
                    
                    document.body.removeChild(logInForm);
                }
                else {
                    logInForm.classList.add("error");
                    logInForm.classList.remove("pending");
                    
                    const spanError = event.target.querySelector("span.error") || document.createElement("span");
                    spanError.classList.add("error");
                    spanError.innerText = "Invalid credentials.";
                    event.target.appendChild(spanError);
                }
            }).catch((error) => {
                logInForm.classList.add("error");
                logInForm.classList.remove("pending");
                
                const spanError = event.target.querySelector("span.error") || document.createElement("span");
                spanError.classList.add("error");
                spanError.innerText = "Server or Plus error: " + error;
                event.target.appendChild(spanError);
            });
        });
        
        return logInForm;
    }
    
    update() {
        if(this.authenticated) {
            this.enableCompose();
            document.body.dataset.authenticated = true;
        }
        else {
            this.disableCompose();
            document.body.dataset.authenticated = false;
        }
        
        if(this.composeObjects) {
            if(this.feed.getHashProperty().post) {
                this.composeObjects.inFeedCompose.classList.remove("visible");
                this.composeObjects.floatingCompose.classList.remove("visible");
            }
            else {
                this.composeObjects.inFeedCompose.classList.add("visible");
                this.composeObjects.floatingCompose.classList.add("visible");
            }
            
            this.updateCurrentInListView();
        }
    }
    
    getEncodedId(id) {
        return btoa(encodeURI(id));
    }
    
    getPostUrl(server, postId) {
        if(server === this.mainServer) {
            return server + window.location.search + "#post=" + this.getEncodedId(postId);
        }
        return server + "#post=" + this.getEncodedId(postId);
    }
    
    generateListStructure(list, append) {
        if(this.listElement) {
            if(!append) {
                this.listElement.innerHTML = "";
            }
            const listWrapper = this.listElement.querySelector("ul") || document.createElement("ul");
            
            window.setTimeout(() => {
                for(let i = list.length - (append ? [...listWrapper.querySelectorAll("li")].length : 1); i >= 0; i--) {
                    const listItem = document.createElement("li");
                    const listItemLink = document.createElement("a");
                    let date = list[i].itemId.match(/\d+/)[0];
                    let dateObj = new Date(date.slice(0, 4) + "-" + date.slice(4, 6) + "-" + date.slice(6, 8));
                    //listItemLink.innerText = dateObj.getDate() + ". " + (dateObj.getMonth() + 1) + ". " + dateObj.getFullYear();
                    listItemLink.innerText = dateObj.toLocaleDateString(undefined, { year: "numeric", month: "long", day: "numeric" });
                    listItemLink.href = this.getPostUrl(list[i].source.server, list[i].itemId);
                    listItem.appendChild(listItemLink);
                    if(list[i].source.server !== this.mainServer) {
                        listItem.classList.add("external");
                    }
                    listWrapper.appendChild(listItem);
                }
                this.listElement.appendChild(listWrapper);
                
                this.updateCurrentInListView();
            }, 1000);
        }
    }
    
    updateCurrentInListView() {
        const allPostLinks = this.listElement.querySelectorAll("a");
        for(let i = 0; i < allPostLinks.length; i++) {
            allPostLinks[i].classList.remove("current");
        }
        const currentPostLinks = this.listElement.querySelectorAll("a[href*=\"#post=" + this.feed.getHashProperty().post + "\"]");
        for(let i = 0; i < currentPostLinks.length; i++) {
            currentPostLinks[i].classList.add("current");
        }
    }
    
    getErrorMessageComponent() {
        let errorMessage = this.feedElement.querySelector("span.error") || document.createElement("span");
        errorMessage.classList.add("error");
        this.feedElement.appendChild(errorMessage);
        return errorMessage;
    }
    
    removeErrorMessageComponent() {
        let errorMessage = this.feedElement.querySelector("span.error");
        if(errorMessage) {
            errorMessage.parentNode.removeChild(errorMessage);
        }
    }
    
    showErrorMessage(text) {
        this.getErrorMessageComponent().innerText = text;
    }
    
    submitData(data) {
        return new Promise((resolve, reject) => {
            this.serverRequest({
                method: "post",
                contentType: "application/json",
                content: JSON.stringify(data)
            }).then((postData) => {
                if(data.create.id) {
                    this.feed.getSource(this.mainServer).updatePost(postData);
                }
                else {
                    this.feed.getSource(this.mainServer).addPost(postData);
                }
                resolve();
            });
        });
    }
    
    submitPost(postData) {
        this.submitData({
            create: postData.send
        }).then(() => {
            postData.onSubmit && postData.onSubmit();
        });
    }
    
    getLinks(string) {
        const urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return string.match(urlRegex);
    }
    
    convertMarkupToTags(string) {
        // Make text wrapped in between asterixes (*) bold
        const boldTextMatches = string.match(/\s+\*([^\*]+)\*\s+/g);
        (boldTextMatches || []).forEach((boldMatch) => {
            string = string.replace(boldMatch, "<strong>" + boldMatch.replace(/\*/g, "") + "</strong>");
        });
        
        // Make text wrapped in between dashes (-) strikethrough
        const strikeTextMatches = string.match(/\s+-([^-]+)-\s+/g);
        (strikeTextMatches || []).forEach((strikeMatch) => {
            string = string.replace(strikeMatch, "<del>" + strikeMatch.replace(/-/g, "") + "</del>");
        });
        
        // Make text wrapped in between underscores (_) italic
        const italicTextMatches = string.match(/\s+_([^_]+)_\s+/g);
        (italicTextMatches || []).forEach((italicMatch) => {
            string = string.replace(italicMatch, "<em>" + italicMatch.replace(/_/g, "") + "</em>");
        });
        
        return string;
    }
    
    generateComposeInFeed() {
        this.composeObjects.inFeedCompose = document.createElement("article");
        this.composeObjects.inFeedCompose.classList.add("compose", "visible");
        
        let input = document.createElement("div");
        input.innerText = this.initialComposeText;
        input.classList.add("input");
        
        this.composeObjects.inFeedCompose.appendChild(input);
        this.feedElement.insertBefore(this.composeObjects.inFeedCompose, this.feedElement.querySelector(".posts") || this.feedElement.firstElementChild);
        
        this.composeObjects.inFeedCompose.addEventListener("click", () => {
            this.displayComposePopUp();
        });
    }
    
    generateFoatingCompose() {
        this.composeObjects.floatingCompose = document.createElement("span");
        this.composeObjects.floatingCompose.classList.add("floating-compose");
        this.composeObjects.floatingCompose.title = "Create a new post";
        this.container.appendChild(this.composeObjects.floatingCompose);
        
        this.composeObjects.floatingCompose.addEventListener("click", () => {
            this.displayComposePopUp();
        });
    }
    
    generateComposePopUp() {
        this.composeObjects.popUpSpace = document.createElement("div");
        this.composeObjects.popUpSpace.classList.add("compose-pop-up-space");
        
        let popUpFeed = document.createElement("div");
        popUpFeed.classList.add("feed");
        
        let composePopUp = document.createElement("article");
        composePopUp.classList.add("compose");
        
        this.composeObjects.textarea = document.createElement("div");
        this.composeObjects.textarea.classList.add("textarea");
        this.composeObjects.textarea.contentEditable = true;
        this.composeObjects.textarea.dataset.placeholder = "Type in post content";
        this.composeObjects.textarea.innerText = this.initialComposeText;
        
        this.composeObjects.mediaWrapper = document.createElement("div");
        this.composeObjects.mediaWrapper.classList.add("link-wrapper", "hidden");
        
        this.composeObjects.clearLink = document.createElement("div");
        this.composeObjects.clearLink.innerText = "Clear link";
        this.composeObjects.clearLink.title = "Clear link";
        this.composeObjects.clearLink.classList.add("clear-link", "hidden");
        this.composeObjects.mediaWrapper.appendChild(this.composeObjects.clearLink);
        
        this.composeObjects.clearLink.addEventListener("click", (event) => {
            this.fillInPageMetadata({});
        });
        
        this.composeObjects.url = null;
        this.composeObjects.title = null;
        this.composeObjects.imageUrl = null;
        
        this.composeObjects.mediaContent = document.createElement("div");
        this.composeObjects.mediaContent.classList.add("preview-wrapper");
        
        this.composeObjects.titleText = document.createElement("span");
        this.composeObjects.titleText.classList.add("title");
        this.composeObjects.mediaContent.appendChild(this.composeObjects.titleText);
        
        this.composeObjects.imagePreview = document.createElement("img");
        this.composeObjects.imagePreview.classList.add("preview");
        this.composeObjects.mediaContent.appendChild(this.composeObjects.imagePreview);
        
        this.composeObjects.mediaWrapper.appendChild(this.composeObjects.mediaContent);
        
        this.composeObjects.uploadFile = document.createElement("input");
        this.composeObjects.uploadFile.type = "file";
        this.composeObjects.uploadFile.classList.add("upload-file");
        
        this.composeObjects.uploadFile.addEventListener("change", (event) => {
            this.submitFile().then((response) => {
                if(response && response.success && response.basename) {
                    this.fillInPageMetadata({
                        imageUrl: this.mainServer + "/uploads/" + response.basename
                    });
                }
            });
        });
        
        this.composeObjects.reshare = document.createElement("input");
        this.composeObjects.reshare.type = "text";
        this.composeObjects.reshare.classList.add("reshare");
        this.composeObjects.reshare.placeholder = "Paste in reshare data to reshare an existing post.";
        
        let tools = document.createElement("div");
        tools.classList.add("tools");
        
        this.composeObjects.publicToggle = document.createElement("div");
        this.composeObjects.publicToggle.classList.add("public-toggle", "button");
        this.composeObjects.publicToggle.dataset.public = false;
        
        this.composeObjects.cancelButton = document.createElement("div");
        this.composeObjects.cancelButton.innerText = "Cancel";
        this.composeObjects.cancelButton.classList.add("cancel", "button");
        
        this.composeObjects.submitButton = document.createElement("div");
        this.composeObjects.submitButton.innerText = "Submit";
        this.composeObjects.submitButton.classList.add("submit", "button", "disabled");
        
        composePopUp.appendChild(this.composeObjects.textarea);
        composePopUp.appendChild(this.composeObjects.mediaWrapper);
        composePopUp.appendChild(this.composeObjects.uploadFile);
        composePopUp.appendChild(this.composeObjects.reshare);
        
        tools.appendChild(this.composeObjects.publicToggle);
        tools.appendChild(this.composeObjects.cancelButton);
        tools.appendChild(this.composeObjects.submitButton);
        
        composePopUp.appendChild(tools);
        popUpFeed.appendChild(composePopUp);
        
        this.composeObjects.popUpSpace.appendChild(popUpFeed);
        this.container.appendChild(this.composeObjects.popUpSpace);
    }
    
    displayComposePopUp(data) {
        data = data || {};
        this.composeObjects.popUpSpace.classList.add("visible");
        
        if(data.edit) {
            if(data.editId) {
                this.composeObjects.editId = data.editId;
            }
            if(data.edit.creationTime) {
                this.composeObjects.creationTime = data.edit.creationTime;
            }
            
            let content = data.edit.content || "";
            
            // Convert known tags to markup
            content = content.replace(/<\/*(b|strong)>/g, "*");
            content = content.replace(/<\/*(i|em)>/g, "_");
            content = content.replace(/<\/*(del)>/g, "-");
            
            // Remove a tags for editing
            content = content.replace(/<\/*(a[^>]*")>/g, "");
            
            this.composeObjects.textarea.innerHTML = content;
            
            this.fillInPageMetadata(data.edit.link || {});
            
            if(data.edit.resharedPost) {
                this.composeObjects.reshare.value = JSON.stringify(data.edit.resharedPost);
            }
            else {
                this.composeObjects.reshare.value = "";
            }
            
            this.composeObjects.submitButton.classList.remove("disabled");
            this.composeObjects.textChanged = true;
            
            this.composeObjects.publicToggle.dataset.public = data.edit.isPublic;
        }
        else {
            this.composeObjects.textarea.innerText = this.initialComposeText;
            this.composeObjects.publicToggle.dataset.public = false;
            
            this.fillInPageMetadata({});
            
            if(data.fillReShare) {
                this.composeObjects.reshare.value = data.fillReShare;
            }
            else {
                this.composeObjects.reshare.value = "";
            }
            
            this.composeObjects.textChanged = false;
        }
        
        this.composeObjects.textarea.focus();
    }
    
    fillInPageMetadata(data) {
        data = data || {};
        
        const images = [];
        if(data.imageUrl) {
            images.push(data.imageUrl);
        }
        if(data.pageImages) {
            data.pageImages.forEach((image) => {
                if(images.indexOf(image) < 0) { // Do not include image duplicates
                    images.push(image);
                }
            });
        }
        
        this.composeObjects.url = data.url || null;
        this.composeObjects.title = data.title || null;
        this.composeObjects.imageUrl = images.length > 0 ? images[0] : null;
        
        if(this.composeObjects.url) {
            this.composeObjects.titleLink = this.composeObjects.titleLink || document.createElement("a");
            this.composeObjects.titleLink.href = this.composeObjects.url;
            this.composeObjects.titleLink.target = "_blank";
            
            this.composeObjects.mediaWrapper.appendChild(this.composeObjects.titleLink);
            this.composeObjects.titleLink.appendChild(this.composeObjects.mediaContent);
        }
        
        this.composeObjects.titleText.innerText = data.title || "";
        this.composeObjects.imagePreview.src = this.composeObjects.imageUrl || "";
        
        const previousImageButton = this.composeObjects.mediaWrapper.querySelector(".previous-image") || document.createElement("div");
        previousImageButton.innerText = "Previous image";
        previousImageButton.title = "Previous image";
        previousImageButton.classList.add("swap-btn");
        previousImageButton.classList.add("previous-image");
        this.composeObjects.mediaWrapper.insertBefore(previousImageButton, this.composeObjects.clearLink.nextSibling);
        
        const nextImageButton = this.composeObjects.mediaWrapper.querySelector(".next-image") || document.createElement("div");
        nextImageButton.innerText = "Next image";
        nextImageButton.title = "Next image";
        nextImageButton.classList.add("swap-btn");
        nextImageButton.classList.add("next-image");
        this.composeObjects.mediaWrapper.insertBefore(nextImageButton, previousImageButton.nextSibling);
        
        let index = 0;
        nextImageButton.addEventListener("click", (event) => {
            index = index < images.length ? index + 1 : 0;
            this.composeObjects.imageUrl = images[index];
            this.composeObjects.imagePreview.src = this.composeObjects.imageUrl || "";
        });
        
        previousImageButton.addEventListener("click", (event) => {
            index = index > 0 ? index - 1 : images.length - 1;
            this.composeObjects.imageUrl = images[index];
            this.composeObjects.imagePreview.src = this.composeObjects.imageUrl || "";
        });
        
        const clearBlock = this.composeObjects.mediaWrapper.querySelector(".clear-block") || document.createElement("div");
        clearBlock.classList.add("clear-block");
        this.composeObjects.mediaWrapper.insertBefore(clearBlock, nextImageButton.nextSibling);
        
        if(Object.keys(data).length > 0) {
            this.composeObjects.mediaWrapper.classList.remove("hidden");
            
            if(images.length <= 1) {
                previousImageButton.classList.add("hidden");
                nextImageButton.classList.add("hidden");
            }
            else {
                previousImageButton.classList.remove("hidden");
                nextImageButton.classList.remove("hidden");
            }
            
            if(this.composeObjects.url || data.title || this.composeObjects.imageUrl) {
                this.composeObjects.mediaWrapper.classList.remove("hidden");
            }
            else {
                this.composeObjects.mediaWrapper.classList.add("hidden");
            }
        }
        else {
            this.composeObjects.mediaWrapper.classList.add("hidden");
        }
        
        this.lastDescriptionUrl = this.composeObjects.url;
    }
    
    enableCompose() {
        if(!this.composeCreated) {
            this.composeObjects = {};
            this.generateComposePopUp();
            this.generateComposeInFeed();
            this.generateFoatingCompose();
            this.composeCreated = true;
            
            this.composeObjects.publicToggle.addEventListener("click", () => {
                this.composeObjects.publicToggle.dataset.public = this.composeObjects.publicToggle.dataset.public == "false";
            });
            
            this.composeObjects.publicToggle.addEventListener("focus", () => {
                this.composeObjects.publicToggle.dataset.public = this.composeObjects.publicToggle.dataset.public == "false";
            });
            
            this.composeObjects.textarea.addEventListener("focus", () => {
               if(this.composeObjects.textarea.innerText === this.initialComposeText) {
                   this.composeObjects.textarea.innerText = "";
               }
            });
            
            this.composeObjects.cancelButton.addEventListener("click", () => {
               this.composeObjects.popUpSpace.classList.remove("visible");
            });
            
            document.addEventListener("keydown", (event) => {
                if(event.key === "Escape") {
                    this.composeObjects.popUpSpace.classList.remove("visible");
                }
            });
            
            this.composeObjects.textarea.addEventListener("input", (event) => {
                const textAreaContent = this.composeObjects.textarea.innerText.trim();
                
                if(textAreaContent.length === 0 || textAreaContent === this.initialComposeText) {
                    this.composeObjects.submitButton.classList.add("disabled");
                }
                else {
                    this.composeObjects.submitButton.classList.remove("disabled");
                }
                this.composeObjects.textChanged = true;
                
                const links = this.getLinks(textAreaContent);
                
                if(links && links.length > 0) {
                    if(!this.lastDescriptionUrl || this.lastDescriptionUrl !== links[0]) {
                        if(this.descriptionTimeout) {
                            clearTimeout(this.descriptionTimeout);
                        }
                        
                        this.descriptionTimeout = window.setTimeout(() => {
                            this.getPageMeta(links[0]).then((data) => {
                                data = data || {};
                                data.url = data.url || links[0];
                                this.fillInPageMetadata(data);
                            });
                        }, 2000);
                        
                        this.lastDescriptionUrl = links[0];
                    }
                }
                else {
                    this.fillInPageMetadata({
                        imageUrl: this.composeObjects.imageUrl || null // Keep image if all is lost
                    });
                }
            });
            
            this.composeObjects.submitButton.addEventListener("click", () => {
                let content = this.composeObjects.textarea.innerText.trim();
                if(content.length > 0 && this.composeObjects.textChanged) {
                    content = content.replace(/\r\n|\n|\r/gm, "<br>");
                    
                    const links = this.getLinks(content);
                    links && links.forEach((link, index) => {
                        content = content.replace(link, "{{LINKPLACEHOLDERPLUSPLUS" + index + "}}");
                    });
                    
                    content = this.convertMarkupToTags(content);
                    
                    links && links.forEach((link, index) => {
                        content = content.replace("{{LINKPLACEHOLDERPLUSPLUS" + index + "}}", "<a href=\"" + link + "\">" + link + "</a>");
                    });
                    
                    this.submitFile().then((response) => {
                        if(response && response.success && response.basename) {
                            this.fillInPageMetadata({
                                imageUrl: this.mainServer + "/uploads/" + response.basename
                            });
                        }
                        
                        this.submitPostContent({
                            content
                        });
                    });
                }
            });
        }
    }
    
    submitFile() {
        if(!this.fileUploadInProgress && this.composeObjects.uploadFile.files.length > 0) {
            return new Promise((resolve, reject) => {
                this.fileUploadInProgress = true;
                
                const formData = new FormData();
                formData.append("file", this.composeObjects.uploadFile.files[0]);
                
                return this.serverRequest({
                    method: "post",
                    content: formData
                }).then((data) => {
                    this.fileUploadInProgress = false;
                    resolve(data);
                }).catch((error) => {
                    this.fileUploadInProgress = false;
                    reject(error);
                });
            });
        }
        return new Promise((resolve, reject) => {
            resolve(null);
        });
    }
    
    submitPostContent(data) {
        data = data || {};
        
        return this.submitPost({
            send: {
                author: this.author,
                server: this.mainServer,
                content: data.content,
                resharedPost: this.composeObjects.reshare.value ? JSON.parse(this.composeObjects.reshare.value) : "",
                link: {
                    title: this.composeObjects.title,
                    url: this.composeObjects.url,
                    imageUrl: this.composeObjects.imageUrl
                },
                postAcl: {
                    isPublic: this.composeObjects.publicToggle.dataset.public == "true"
                },
                id: this.composeObjects.editId ? this.composeObjects.editId : null,
                creationTime: this.composeObjects.creationTime ? this.composeObjects.creationTime : null
            },
            onSubmit: () => {
                this.composeObjects.popUpSpace.classList.remove("visible");
            }
        });
    }
    
    disableCompose() {
        if(this.composeCreated) {
            this.composeObjects.inFeedCompose.parentNode.removeChild(this.composeObjects.inFeedCompose);
            this.composeObjects.floatingCompose.parentNode.removeChild(this.composeObjects.floatingCompose);
            this.composeObjects.popUpSpace.parentNode.removeChild(this.composeObjects.popUpSpace);
            this.composeCreated = false;
        }
    }
    
    getPageMeta(url) {
        return new Promise((resolve, reject) => {
            this.serverRequest({
                queryString: "?meta=" + encodeURI(url),
                method: "get"
            }).then((data) => {
                resolve(data);
            }).catch((error) => {
                console.error("Unable to fetch page meta data");
                reject(error);
            });
        });
    }
}

let plus = new Plus();
