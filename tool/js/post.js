"use strict";

class Post {
    constructor(data) {
        this.plus = data.plus || {};
        this.ownFeed = data.ownFeed || {};
        this.itemId = data.itemId || null;
        this.server = data.server || this.plus.mainServer;
        this.hash = btoa(encodeURI(data.itemId || ""));
        this.feed = data.feed || document.body;
        this.prepend = data.prepend || false;
        this.virtual = data.virtual || false;
        this.onLoad = data.onLoad || null;
        this.onError = data.onError || null;
        this.postGoogle = false;
        
        this.passQuery = this.server === this.plus.mainServer;
        this.reshared = this.itemId ? false : true;
        
        this.construct();
        
        if(this.itemId) {
            this.fetchData();
        }
        else if(data.content) {
            this.populate(data.content);
        }
    }
    
    fetchData() {
        let url = this.plus.getServerComponent(this.server);
        if(this.server === this.plus.mainServer) {
            url += "?key=" + this.plus.key;
        }
        
        url += (url.match(/\?/) ? "&" : "?") + "post-id=" + encodeURIComponent(this.itemId);
        
        if(this.passQuery) {
            url += url.match(/\?/) ? window.location.search.replace(/\?/, "&") : window.location.search;
        }
        
        fetch(url).then((response) => {
            return response.json();
        })
        .then((data) => {
            this.populate(data);
        })
        .catch((error) => {
            this.onError && this.onError(error);
        });
    }
    
    deletePost() {
        return new Promise((resolve, reject) => {
            let url = this.plus.getServerComponent(this.server);
            if(this.server === this.plus.mainServer) {
                url += "?key=" + this.plus.key;
            }
            if(this.passQuery) {
                url += url.match(/\?/) ? window.location.search.replace(/\?/, "&") : window.location.search;
            }
            
            fetch(url, {
                method: "post",
                headers: {
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    delete: this.itemId
                })
            }).then((response) => {
                return response.json();
            }).then((data) => {
                if(this.ownFeed) {
                    this.ownFeed.getSource(this.server).removePost(data);
                }
                resolve();
            }).catch((error) => {
                this.onError && this.onError(error);
                reject();
            });
        });
    }
    
    editPost() {
        return new Promise((resolve, reject) => {
            let url = this.plus.getServerComponent(this.server);
            if(this.server === this.plus.mainServer) {
                url += "?key=" + this.plus.key;
            }
            
            url += (url.match(/\?/) ? "&" : "?") + "post-id=" + encodeURIComponent(this.itemId);
            
            if(this.passQuery) {
                url += url.match(/\?/) ? window.location.search.replace(/\?/, "&") : window.location.search;
            }
            
            fetch(url).then((response) => {
                return response.json();
            }).then((data) => {
                this.plus.displayComposePopUp({
                    edit: data,
                    editId: this.itemId
                });
                resolve();
            }).catch((error) => {
                this.onError && this.onError(error);
                reject();
            });
        });
    }
    
    createMainStructure() {
        this.elements.main = {};
        
        this.elements.main.topHead = document.createElement("div");
        this.elements.main.topHead.classList.add("head");
        
        this.elements.main.contentWrapper = document.createElement("div");
        this.elements.main.contentWrapper.classList.add("content");
        
        this.elements.main.additionalInfo = document.createElement("div");
        this.elements.main.additionalInfo.classList.add("additional-info-wrapper");
        
        this.elements.main.resharedPostWrapper = document.createElement("div");
        this.elements.main.resharedPostWrapper.classList.add("reshared-post");
        
        Object.keys(this.elements.main).forEach((item) => {
            this.postContainer.appendChild(this.elements.main[item]);
        });
        
        this.elements.main.toolbar = document.createElement("div");
        this.elements.main.toolbar.classList.add("tools");
    }
    
    createParts() {
        this.elements.parts = {};
        
        this.elements.parts.heading = document.createElement("h2");
        this.postContainer.appendChild(this.elements.parts.heading);
        
        this.elements.parts.reshareElement = document.createElement("input");
        this.elements.parts.reshareElement.type = "text";
        this.elements.parts.reshareElement.classList.add("reshare");
        this.postContainer.appendChild(this.elements.parts.reshareElement);
        
        this.elements.parts.author = document.createElement("a");
        this.elements.parts.author.classList.add("author");
        this.elements.main.topHead.appendChild(this.elements.parts.author);
        
        this.elements.parts.date = document.createElement("span");        
        if(!this.reshared) {
            this.elements.parts.dateLink = document.createElement("a");
            this.elements.parts.dateLink.classList.add("date");
            this.elements.parts.date.classList.add("date-label");
            this.elements.main.topHead.appendChild(this.elements.parts.dateLink);
            this.elements.parts.dateLink.appendChild(this.elements.parts.date);
        }
        else {
            this.elements.parts.date.classList.add("date");
            this.elements.main.topHead.appendChild(this.elements.parts.date);
        }
        
        this.elements.parts.authorImage = document.createElement("img");
        this.elements.parts.author.appendChild(this.elements.parts.authorImage);
        
        this.elements.parts.authorLabel = document.createElement("span");
        this.elements.parts.author.appendChild(this.elements.parts.authorLabel);
        
        this.elements.parts.content = document.createElement("p");
        this.elements.main.contentWrapper.appendChild(this.elements.parts.content);
        //this.elements.main.contentWrapper.appendChild(this.elements.main[item]);
        
        this.elements.parts.mediaContent = document.createElement("div");
        this.elements.main.additionalInfo.appendChild(this.elements.parts.mediaContent);
        
        this.elements.parts.mediaContentText = document.createElement("span");
        this.elements.parts.mediaContent.appendChild(this.elements.parts.mediaContentText);
        
        this.elements.parts.image = document.createElement("img");
        this.elements.parts.mediaContent.appendChild(this.elements.parts.image);
        
        this.elements.parts.shareButton = document.createElement("div");
        this.elements.parts.shareButton.innerHTML = '<svg viewBox="0 0 24 24" height="100%" width="100%"><path class="Ce1Y1c" d="M18 16.08c-.76 0-1.44.3-1.96.77L8.91 12.7c.05-.23.09-.46.09-.7s-.04-.47-.09-.7 l7.05-4.11c.54.5 1.25.81 2.04.81 1.66 0 3-1.34 3-3s-1.34-3-3-3-3 1.34-3 3c0 .24.04.47.09.7L8.04 9.81C7.5 9.31 6.79 9 6 9c-1.66 0-3 1.34-3 3s1.34 3 3 3 c.79 0 1.5-.31 2.04-.81l7.12 4.16c-.05.21-.08.43-.08.65 0 1.61 1.31 2.92 2.92 2.92 1.61 0 2.92-1.31 2.92-2.92s-1.31-2.92-2.92-2.92z"></path></svg>';
        this.elements.parts.shareButton.classList.add("share", "button", "round");
        this.elements.main.toolbar.appendChild(this.elements.parts.shareButton);
        
        this.elements.parts.editButton = document.createElement("span");
        this.elements.parts.editButton.innerText = "Edit post";
        this.elements.parts.editButton.title = "Edit post";
        this.elements.parts.editButton.classList.add("edit", "button", "round");
        
        this.elements.parts.deleteButton = document.createElement("span");
        this.elements.parts.deleteButton.innerText = "Delete post";
        this.elements.parts.deleteButton.title = "Delete post";
        this.elements.parts.deleteButton.classList.add("delete", "button", "round");
    }
    
    createElements() {
        this.elements = {};
        this.createMainStructure();
        this.createParts();
    }
    
    construct() {
        this.postContainer = document.createElement("article");
        this.createElements();
        
        this.elements.parts.deleteButton.addEventListener("click", () => {
            if(window.confirm("Do you really want to delete this post?")) {
                this.deletePost();
            }
        });
        
        this.elements.parts.editButton.addEventListener("click", () => {
            this.editPost();
        });
                
        if(!this.virtual) {
            if(this.prepend) {
                this.feed.insertBefore(this.postContainer, this.prepend === true ? this.feed.firstChild : this.prepend);
            }
            else {
                this.feed.appendChild(this.postContainer);
            }
        }
    }
    
    populate(data) {
        data = data || {};
        
        if(data.error) {
            this.postContainer.classList.add("hidden");
            console.error(data.error);
            this.onError && this.onError(data.error);
            return false;
        }
        
        if(data.server) {
            let query = (data.server === this.plus.mainServer ? window.location.search : "");
            this.permalink = data.server + query + "#post=" + this.hash;
        }
        else if(this.hash) {
            let query = (this.server === this.plus.mainServer ? window.location.search : "");
            this.permalink = this.server + query + "#post=" + this.hash;
        }
        else {
            this.permalink = "#";
        }
        
        this.postGoogle = data.postGoogle === true;
        if(!this.reshared) {
            this.postContainer.appendChild(this.elements.main.toolbar);
            
            if(this.plus.authenticated) {
                if(this.postGoogle && this.server === this.plus.mainServer) {
                    this.elements.main.toolbar.appendChild(this.elements.parts.editButton);
                    this.elements.main.toolbar.appendChild(this.elements.parts.deleteButton);
                    this.postContainer.classList.add("deletable");
                }
                
                if(!data.isPublic) {
                    this.postContainer.classList.add("private");
                }
            }
        }
        
        /*
        if(this.plus) {
            this.plus.setAuthor({
                reshared: this.reshared,
                author: data.author
            });
        }
        */
        
        this.postContainer.dataset.id = data.author.displayName;
        this.postContainer.dataset.author = this.itemId;
        
        this.elements.parts.heading.innerText = this.hash || "Unlabeled";
        this.elements.parts.authorLabel.innerText = this.reshared ? "Previously shared by " + data.author.displayName : data.author.displayName;
        this.elements.parts.author.title = data.author.displayName;
        this.elements.parts.author.href = this.reshared && this.hash ? this.permalink : data.author.profilePageUrl;
        this.elements.parts.authorImage.src = data.author.avatarImageUrl;
        
        if(this.elements.parts.dateLink) {
            this.elements.parts.dateLink.href = this.permalink;
        }
        
        let date = data.updateTime || data.creationTime;
        if(date) {
            const dateTimeObj = new Date(date);
            // this.elements.parts.date.innerText = new Date(date).toISOString().replace(/(T|Z|.000)/g, " ");
            this.elements.parts.date.innerText = dateTimeObj.toLocaleDateString(undefined, {
                year: "numeric",
                month: "2-digit",
                day: "numeric",
                hour: "2-digit",
                minute: "2-digit",
                second: "2-digit"
            });
            
            if(data.creationTime && date !== data.creationTime) {
                this.elements.parts.date.dataset.created = new Date(data.creationTime).toISOString().replace(/(T|Z|.000)/g, " ");
                this.elements.parts.date.innerText += " (updated)";
                this.elements.parts.date.classList.add("updated");
            }
        }
        
        this.elements.parts.content.innerHTML = data.content || "";
        
        if(data.link) {
            let allowThumbnail = true;
            
            if(data.link.url) {
                if(data.link.url.match(/youtube\.com/)) {
                    // With YouTube video
                    const videoMatch = data.link.url.match(/\?v=(.*)/);
                    const videoId = videoMatch ? videoMatch[1] : "";
                    
                    const youTubeWrapper = document.createElement("div");
                    youTubeWrapper.classList.add("youtube-wrapper");
                    
                    this.elements.parts.iframe = document.createElement("iframe");
                    this.elements.parts.iframe.classList.add("youtube-player");
                    this.elements.parts.iframe.type = "text/html";
                    this.elements.parts.iframe.setAttribute("frameborder", "0");
                    this.elements.parts.iframe.setAttribute("allowFullScreen", "");
                    this.elements.parts.iframe.src = "https://www.youtube.com/embed/" + videoId;
                    
                    youTubeWrapper.appendChild(this.elements.parts.iframe);
                    this.elements.main.additionalInfo.appendChild(youTubeWrapper);
                    
                    allowThumbnail = false;
                }
                else {
                    // With link
                    this.elements.parts.link = document.createElement("a");
                    this.elements.parts.link.target = "_blank";
                    this.elements.parts.link.title = data.link.title;
                    this.elements.parts.link.href = data.link.url;
                    
                    this.elements.parts.mediaContentText.innerText = data.link.title;
                    
                    this.elements.parts.mediaContent.parentNode.appendChild(this.elements.parts.link);
                    this.elements.parts.link.appendChild(this.elements.parts.mediaContent);
                }
            }
            else {
                // No link
            }
            
            if(data.link.imageUrl && allowThumbnail) {
                // There is an image to display
                this.elements.parts.image.src = data.link.imageUrl;
                this.elements.parts.image.onerror = () => { this.elements.parts.image.parentNode.removeChild(this.elements.parts.image); };
            }
        }
        
        if(data.resharedPost) {
            this.resharedPost = new Post({
                ownFeed: this.ownFeed,
                plus: this.plus,
                feed: this.elements.main.resharedPostWrapper,
                content: data.resharedPost
            });
        }
        
        let reshareObj = {};
        Object.assign(reshareObj, data);
        delete reshareObj.resharedPost;
        let reshareString = JSON.stringify(reshareObj);
        this.postContainer.dataset.reshare = reshareString;
        this.elements.parts.reshareElement.value = reshareString;
        
        this.elements.parts.shareButton.addEventListener("click", () => {
            if(this.plus.authenticated) {
                this.plus.displayComposePopUp({
                    fillReShare: reshareString
                });
            }
            else {
                this.elements.parts.reshareElement.select();
                document.execCommand("copy");
                alert("Post copied to clipboard for share");
            }
        });
        
        this.onLoad && this.onLoad();
        
        this.focusPost(true);
        if(this.elements.main.contentWrapper.offsetHeight > 70) {
            this.focusPost(false);
            this.expandMe = document.createElement("div");
            this.expandMe.classList.add("expand");
            this.expandMe.innerText = "Expand/Shrink";
            this.elements.main.contentWrapper.parentNode.insertBefore(this.expandMe, this.elements.main.contentWrapper.nextElementSibling);
            
            this.expandMe.addEventListener("click", () => {
                this.postContainer.classList.toggle("focus");
            });
        }
    }
    
    focusPost(focusIn, reshared) {
        if(focusIn) {
            this.postContainer.classList.add("focus");
        }
        else {
            this.postContainer.classList.remove("focus");
        }
        
        if(reshared && this.resharedPost) {
            this.resharedPost.focusPost(focusIn, reshared);
        }
    }
}
