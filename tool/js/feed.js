"use strict";

class Feed {
    constructor(data) {
        this.index = data.index;
        this.plus = data.plus || {};
        this.servers = data.servers || ["."];
        
        this.sources = [];
        
        this.feed = data.feed || documen.body;
        this.feedParent = this.feed.parentNode || documen.body;
        this.listElement = data.listElement || null;
        
        this.separateLists = [];
        
        this.list = [];
        this.loadedPosts = [];
        this.toLoadLater = [];
        this.lastDisplayedIndex = -1;
        this.sortByNewestUp = true;
        
        this.viewLimit = 10;
        this.scrollSensitivity = 200;
        
        this.init();
    }
    
    init() {
        this.feedContentContainer = document.createElement("div");
        this.feedContentContainer.classList.add("posts");
        this.feed.appendChild(this.feedContentContainer);
        
        this.reset();
    }
    
    reset() {
        this.sources = [];
        this.separateLists = [];
        this.toLoadLater = [];
        
        this.fetchData().then(() => {
            this.lastDisplayedIndex = this.list.length - 1;
            this.selectView();
            this.loadLater();
            this.hashChange();
            this.plus.generateListStructure(this.list);
            
            // this.createReorderButton();
        });
    }
    
    getSource(server) {
        for(let i = 0; i < this.sources.length; i++) {
            if(this.sources[i].server === server) {
                return this.sources[i];
            }
        }
        return null;
    }
    
    sortFeed(newestUp) {
        if(newestUp) {
            this.list.sort((a, b) => {
                const dateStringA = String(a.itemId.match(/^\d+/));
                const dateStringB = String(b.itemId.match(/^\d+/));
                
                const yearMonthDayA = dateStringA.match(/(\d{4})(\d{2})(\d{2})(\d*)/);
                const yearMonthDayB = dateStringB.match(/(\d{4})(\d{2})(\d{2})(\d*)/);
                
                const dateA = new Date(yearMonthDayA[1], yearMonthDayA[2], yearMonthDayA[3]);
                const dateB = new Date(yearMonthDayB[1], yearMonthDayB[2], yearMonthDayB[3]);
                
                const diff = dateA.getTime() - dateB.getTime();
                
                if(diff === 0 && yearMonthDayA[4] && yearMonthDayB[4]) {
                    return yearMonthDayA[4] - yearMonthDayB[4];
                }
                return diff;
            });
        }
        else {
            this.list.sort((a, b) => {
                const dateStringA = String(a.itemId.match(/^\d+/));
                const dateStringB = String(b.itemId.match(/^\d+/));
                
                const yearMonthDayA = dateStringA.match(/(\d{4})(\d{2})(\d{2})(\d*)/);
                const yearMonthDayB = dateStringB.match(/(\d{4})(\d{2})(\d{2})(\d*)/);
                
                const dateA = new Date(yearMonthDayA[1], yearMonthDayA[2], yearMonthDayA[3]);
                const dateB = new Date(yearMonthDayB[1], yearMonthDayB[2], yearMonthDayB[3]);
                
                const diff = dateB.getTime() - dateA.getTime();
                
                if(diff === 0 && yearMonthDayA[4] && yearMonthDayB[4]) {
                    return yearMonthDayB[4] - yearMonthDayA[4];
                }
                return diff;
            });
        }
    }
    
    updateSeparate(index, data) {
        this.separateLists[index] = data;
        
        this.list = [];
        for(let i = 0; i < this.separateLists.length; i++) {
            if(this.separateLists[i]) {
                this.list = this.list.concat(this.separateLists[i]);
            }
        }
        
        this.sortFeed(this.sortByNewestUp);
    }
    
    selectView() {
        this.plus.removeErrorMessageComponent();
        let hash = this.getHashProperty();
        if(hash.post) {
            this.showSingle(hash);
        }
        else {
            this.updateView();
            this.lastDisplayedIndex = this.list.length - 1;
        }
    }
    
    initializeSource(index) {
        return new Promise((resolve, reject) => {
            const source = new SingleSource({
                index: index,
                plus: this.plus,
                server: this.servers[index],
                passQuery: index === 0,
                onLoad: (list, sourceItem) => {
                    this.updateSeparate(index, list);
                    if(index === 0 && sourceItem.authenticated === true) {
                        this.plus.authenticate();
                    }
                    resolve();
                },
                onUpdate: (list, sourceItem, skip) => {
                    this.updateSeparate(index, list);
                    if(index === 0 && sourceItem.authenticated === true) {
                        this.plus.authenticate();
                    }
                    this.lastDisplayedIndex = this.list.length - index;
                    if(!skip) {
                        this.selectView();
                    }
                    else {
                        this.plus.generateListStructure(this.list, true);
                        
                        /* Virtual list gets reordered by date so we can't rely on all the posts to display being at the end, e.g. by .slice(0, 10) */
                        this.list.filter(listItem => listItem.source.server === sourceItem.server).forEach((listItem, index) => {
                            const postAlreadyRendered = !!this.loadedPosts.find(postData => postData.itemId === listItem.itemId);
                            if(!postAlreadyRendered) {
                                const post = this.initializePost({
                                    itemId: listItem.itemId,
                                    server: listItem.source.server
                                });
                            }
                        });
                    }
                    resolve();
                },
                onError: (list, sourceItem) => {
                    this.updateSeparate(index, list);
                    if(index === 0 && sourceItem.authenticated === true) {
                        this.plus.authenticate();
                    }
                    resolve();
                }
            });
            
            this.sources.push(source);
        });
    }
    
    updatereOrderButtonText() {
        if(this.sortByNewestUp) {
            this.reorderButton.innerText = "Reorder list by oldest date";
        }
        else {
            this.reorderButton.innerText = "Reorder list by newest date";
        }
    }
    
    createReorderButton() {
        this.reorderButton = this.reorderButton || document.createElement("div");
        this.reorderButton.classList.add("reorder");
        this.updatereOrderButtonText();
        this.feed.insertBefore(this.reorderButton, this.feed.firstElementChild);
        
        this.reorderButton.addEventListener("click", () => {
            this.sortByNewestUp = !this.sortByNewestUp;
            this.sortFeed(this.sortByNewestUp);
            this.selectView();
            this.updatereOrderButtonText();
        });
    }
    
    fetchData() {
        return new Promise((resolve, reject) => {
            let counter = 0;
            
            if(this.servers.length > 0) {
                this.initializeSource(counter).then((data) => {
                    if(!this.plus.authenticated || this.servers.length === 1) {
                        resolve();
                        return;
                    }
                    
                    counter++;
                    for(let index = 1; index < this.servers.length; index++) {
                        this.initializeSource(index).then(() => {
                            counter++;
                            if(counter >= this.servers.length) {
                                resolve();
                            }
                        });
                    }
                });
            }
        });
    }
    
    initializePost(data) {
        const post = new Post({
            ownFeed: this,
            plus: this.plus,
            feed: this.feedContentContainer,
            itemId: data.itemId,
            server: data.server,
            virtual: data.virtual || false
        });
        this.loadedPosts.push({
            itemId: data.itemId,
            post
        });
        return post;
    }
    
    clearFeed(single) {
        this.single = single === true;
        
        this.loadedPosts = [];
        
        this.feed.removeChild(this.feedContentContainer);
        this.feedContentContainer = document.createElement("div");
        this.feedContentContainer.classList.add("posts");
        this.feed.appendChild(this.feedContentContainer);
    }
    
    /* Takes post ID (without initial hash if it comes from window.loction.hash for instance */
    showSingle(postLink) {
        try {
            postLink = postLink || {};
            let itemId = decodeURI(atob(postLink.post || ""));
            let server = postLink.server || "";
            
            let post = this.initializePost({
                itemId,
                server,
                virtual: true
            });
            
            post.onLoad = () => {
                this.clearFeed(true);
                this.feedContentContainer.appendChild(post.postContainer);
                this.lastDisplayedIndex = this.list.length;
                for(let i = 0; i < this.list.length; i++) {
                    if(this.list[i].itemId === itemId && this.list[i].source.server === server) {
                        this.lastDisplayedIndex = i;
                        break;
                    }
                }
                
                let boundaries = post.postContainer.getBoundingClientRect();
                window.scroll({
                    top: boundaries.top - 100, 
                    left: boundaries.left, 
                    behavior: "smooth"
                });
                
                window.setTimeout(() => {
                    post.focusPost(true, true);
                }, 250);
            }
            
            post.onError = (error) => {
                if(error === "accessNotGranted") {
                    this.plus.showErrorMessage("Access to this post not granted.");
                }
                else {
                    this.plus.showErrorMessage("Nothing to show here, post not found.");
                }
                this.hashInvalid = true;
            }
        } catch(error) {
            console.error("invalid postId", postId);
            this.plus.showErrorMessage("Nothing to show here, invalid post id provided.");
            this.hashInvalid = true;
            
            window.setTimeout(() => {
                history.pushState("", document.title, window.location.pathname);
                this.selectView();
            }, 3000);
        }
    }
    
    hashChange(plusUpdate) {
        window.onhashchange = () => {
            this.hashInvalid = false;
            this.selectView();
            this.plus.update();
        };
    }
    
    hashNotEmpty() {
        return !this.hashInvalid && window.location.hash !== "" && window.location.hash !== "#";
    }
    
    getHashProperty() {
        let data = {};
        if(this.hashNotEmpty()) {
            let post = window.location.hash.match(/[#+]post=([^+]*)/);
            let server = window.location.hash.match(/[#+]server=([^+]*)/);
            let author = window.location.hash.match(/[#+]author=([^+]*)/);
            
            if(post && post.length > 1) {
                data.post = post[1];
            }
            if(server && server.length > 1) {
                data.server = server[1];
            }
            if(author && author.length > 1) {
                data.author = author[1];
            }
        }
        return data;
    }
    
    updateView() {
        this.clearFeed();
        this.toLoadLater = [];
        
        let lastIndex = (this.list.length - 1) - this.lastDisplayedIndex;
        let count = 0;
        for(let index = this.list.length - 1; index >= 0; index--) {
            if(count < this.viewLimit || count <= lastIndex) {
                let post = this.initializePost({
                    itemId: this.list[index].itemId,
                    server: this.list[index].source.server
                });
                
                if(lastIndex > 0 && this.lastDisplayedIndex === index) {
                    post.onLoad = () => {
                        window.setTimeout(() => {
                            post.focusPost(false, true);
                            
                            let boundaries = post.postContainer.getBoundingClientRect();
                            window.scroll({
                                top: boundaries.top - 100, 
                                left: boundaries.left, 
                                behavior: "smooth"
                            });
                        }, lastIndex * 2 + 200);
                    };
                }
            }
            else {
                this.toLoadLater.push({
                    index,
                    call: (prepend) => {
                        return this.initializePost({
                            itemId: this.list[index].itemId,
                            server: this.list[index].source.server
                        });
                    }
                });
            }
            
            count++;
        }
    }
    
    async loadMore() {
        if(!this.single && !this.loadMoreInProgress) {
            this.loadMoreInProgress = true;
            
            for(const source of this.sources) {
                await source.loadMore();
            }
            
            let lastPostContainer = null;
            for(let i = 0; i < this.toLoadLater.length && i < this.viewLimit; i++) {
                const post = this.toLoadLater[i].call(lastPostContainer);
                lastPostContainer = post.postContainer;
                this.toLoadLater.splice(i, 1);
            }
            
            this.loadMoreInProgress = false;
        }
    }
    
    loadLater() {
        const feedContainer = document.querySelector(".feed-container");
        if(feedContainer) {
            feedContainer.addEventListener("scroll", (event) => {
                if(!this.getHashProperty().post) {
                    if(event.target.scrollTop + event.target.offsetHeight + this.scrollSensitivity > event.target.scrollHeight) {
                        this.loadMore();
                    }
                }
            });
            
            window.addEventListener("resize", () => {
                if(feedContainer.scrollHeight < feedContainer.offsetHeight + this.scrollSensitivity) {
                    this.loadMore();
                };
            });
            
            window.setTimeout(() => {
                if(feedContainer.scrollHeight < feedContainer.offsetHeight + this.scrollSensitivity) {
                    this.loadMore();
                };
            }, 1000);
        }
    }
}
