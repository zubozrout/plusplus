# Plus+

Having old Google Plus data laying around with no place to publish these? You can use this tool to put your old content (just posts, no likes or comments) on your web. You can even write new posts, edit and delete those. The tool should also honour privacy settings - so if a post is not public it will only be visible when you log in with defined credentials. But there is also no "share with a particular account or group" feature unfortunately, so you can either share publicly or not share at all.

Additionally you can connect up to other servers running the same code and watch for other people's posts.

This tools does as much as (reasonably) possible on the client, and could for sure use some optimizations. But considering the low usefulness it has I am not planning to change how it works. But be my guest :). You can also finish up the `visor.php` which is not complete and has no use atm, but could help with indvidual posts indexing by search engines. Anyway, at this moment the tool suits my needs well as is.

# Config

You will want to modify config.ini file with your details. Suggesting to use the same display name and avatar image url as the one used with the old Google Plus account for consistency, but it is not mandatory.

Additionally, under [settings] you can add a number of other urls running this code:

```
subscriptions[] = "https://whatever.the/path/to/the/plus+/may/be"
subscriptions[] = "https://different.path/to/the/plus+"
```

and then, once logged in you should see other public posts from these servers too.

# Use old Google Plus posts

In order to load old posts, place all the downloaded json files under /posts folder. Other files are not supported by this tool, so you'll lose all comments and likes to your posts.

# Example

You can currently see this running on my own server here: https://plus.zubozrout.cz

# License

At this stage there are 3 assets which are in dispute - they are from Google (2 logos and one loading gif animation) and I am sure I can't use these but have not gotten to their removal yet. After all this project only exists because of them and it is all based around the idea of Google+. But some day I may replace those.

In addition to these all other assets are obtained from here: https://www.svgrepo.com/vectors/height/ - and were available with no attribution required. So you should be able to use these as such.

As for the rest (that is the code itself - PHP and JS) this is all available under GPLv3.
So if you do modify this code I would appreciate if you could share your modifications with the world so that it can be to everyone's benefit :).
